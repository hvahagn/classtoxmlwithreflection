﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Xml.Linq;

namespace ClassToXmlWithReflection
{
    class Program
    {
        static void Main(string[] args)
        {
          
            List<Student> list=CreateStudentsList(20);
            SaveStudentToXml(list);
            Print(list);
            Console.ReadLine();
        }



        public static List<Student> CreateStudentsList(int count)
        {
            var list = new List<Student>(count);
            Random rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                list.Add(new Student
                {
                    Name=$"A{i}",
                    SurName=$"A{i}yan",
                    age=(byte)rnd.Next(15,50),
                    Email=$"A{i}.A{i}yan@gmail.com"
                });
              
            }
            return list;
        }

        public static void Print(List<Student> list)
        {
            foreach (var item in list)
            {
                Type type = item.GetType();
                foreach (var prop in type.GetProperties())
                {
                    Console.WriteLine(prop.Name+" = "+prop.GetValue(item));
                }
                foreach (var field in type.GetFields())
                {
                    Console.WriteLine(field.Name+" = "+field.GetValue(item));
                }
                Console.WriteLine();
            }
        }
        public static void SaveStudentToXml(List<Student> list)
        {
            XDocument xdoc = new XDocument();
            XElement xmlStudents = new XElement("Students");
            foreach (var item in list)
            {
                Type t = item.GetType();
                XElement xmlStudent = new XElement("Student");
                foreach (var prop in t.GetProperties())
                {
                    xmlStudent.Add(new XElement(prop.Name, prop.GetValue(item)));
                }
                foreach(var prop in t.GetFields())
                {
                    xmlStudent.Add(new XElement(prop.Name, prop.GetValue(item)));
                }
                xmlStudents.Add(xmlStudent);
            }
            xdoc.Add(xmlStudents);
            xdoc.Save("Students.xml");
        }





    }
}
